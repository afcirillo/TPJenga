package Ej1;

import java.util.ArrayList;
import java.util.Random;

public class Torre {
	
	ArrayList<Niveles> torre;
	
	Torre(int cantNiveles){		//CONSTRUCTOR-------------------------------
		this.torre = new ArrayList<Niveles>();
		this.agregarNivelesIniciales(cantNiveles);
	}
	
	public int tama�o(){
		return torre.size();
	}
	
	public void agregarNivelesIniciales(int n) {
		for(int i=0; i<n;i++){
			this.a�adirNivel(1, 1, 1);	
		}
	}
	
	public void a�adirNivel(int pi, int pd, int pc){
		Niveles nivel = new Niveles(pi, pd, pc,this.tama�o());
		torre.add(nivel);
	}
	
	public void mover(int numNivel, int palito){	//QUITAR y A�ADIR PALITOS-----------------------------
		switch (palito) {
			case 0 :
				this.torre.get(numNivel).setPalIzq(0);
				break;
			case 1 :
				this.torre.get(numNivel).setPalCen(0);
				break;
			case 2 :
				this.torre.get(numNivel).setPalDer(0);
				break;
		}
		if(cantPalitos(this.tama�o()-1)==3){
			this.a�adirNivel(1,0,0);
		}else{
			if(this.torre.get(this.tama�o()-1).getPalIzq()==0){
				this.torre.get(this.tama�o()-1).setPalIzq(1);
			}else if(this.torre.get(this.tama�o()-1).getPalCen()==0){
				this.torre.get(this.tama�o()-1).setPalCen(1);
			}else{
				this.torre.get(this.tama�o()-1).setPalDer(1);
			}
		}
	}
	
	public int cantPalitos(int numNivel){		//CUENTA LA CANTIDAD DE PALITOS---------------------------
		int cont=0;
		if(this.torre.get(numNivel).getPalIzq()==1){
			cont++;	
		}
		if(this.torre.get(numNivel).getPalCen()==1){
			cont++;	
		}
		if(this.torre.get(numNivel).getPalDer()==1){
			cont++;	
		}
		return cont;
	}
	
	
	public int primerNivelPosible(){
		int nivelRecomendado = -1;
		boolean tresPalitos = false;
		for (int i =0; i<tama�o()-1;i++){
			if (cantPalitos(this.torre.get(i).getNivel())==3){
				nivelRecomendado = this.torre.get(i).getNivel();
				i = tama�o();
				tresPalitos = true;
			}
		}
		if (tresPalitos == false){
			for (int i =0; i<tama�o()-1;i++){
				if (cantPalitos(this.torre.get(i).getNivel())==2){
					nivelRecomendado = this.torre.get(i).getNivel();
					i = tama�o();
				}
			}
		}
		if (nivelRecomendado == -1){
			nivelRecomendado = 0;
		}
		return nivelRecomendado;
	}
	
	int piezaRecomendada(int numNivel){		
		int palitoRecomendado = -1;
		Random r = new Random();
		if (cantPalitos(numNivel)==3){
			int palito = r.nextInt(3);
			palitoRecomendado = palito;
		}		
		if (cantPalitos(numNivel)==2){
			if (this.torre.get(numNivel).getPalIzq()==1 && this.torre.get(numNivel).getPalCen()==1 && this.torre.get(numNivel).getPalDer()==0){
				palitoRecomendado = 0;
			}else if (this.torre.get(numNivel).getPalIzq()==0 && this.torre.get(numNivel).getPalCen()==1 && this.torre.get(numNivel).getPalDer()==1){
				palitoRecomendado = 2;
			}else if (this.torre.get(numNivel).getPalIzq()==1 && this.torre.get(numNivel).getPalCen()==0 && this.torre.get(numNivel).getPalDer()==1){
				palitoRecomendado = 0;
			}
		}		
		return palitoRecomendado;
	}

	@Override
	public String toString() {
		StringBuilder mostrarTorre = new StringBuilder();
		for(int i=this.tama�o()-1;i>-1;i--){
			mostrarTorre.append(torre.get(i).toString());
		}
		return mostrarTorre.toString();
	}
	
	
	public void a�adirPalito(){		//A�ADIR PALITO PARA EL TEST
		if(cantPalitos(this.tama�o()-1)==3){
			this.a�adirNivel(1,0,0);
		}else{
			if(this.torre.get(this.tama�o()-1).getPalIzq()==0){
				this.torre.get(this.tama�o()-1).setPalIzq(1);
			}else if(this.torre.get(this.tama�o()-1).getPalCen()==0){
				this.torre.get(this.tama�o()-1).setPalCen(1);
			}else{
				this.torre.get(this.tama�o()-1).setPalDer(1);
			}
		}
	}
}