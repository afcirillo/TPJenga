package Ej1;

import java.util.ArrayList;
import java.util.Random;

public class Jenga {
	
	public Torre torre;
	private ArrayList<String> jugadores = new ArrayList<String>();
	private String JugActual="";
	private ArrayList<String> ganadores= new ArrayList<String>();
	private int indice;
	
	Jenga(int cantNiveles, ArrayList<String> jugadores){		//CONSTRUCTOR---------------------------
		torre = new Torre(cantNiveles);
		this.jugadores = jugadores;
		this.indice = 0;
	}
	
	public void Jugar(){	
		System.out.println("Jenga:");
		System.out.println(torre.toString());		
		while(this.ganadores.isEmpty()){
			JugActual=this.jugadores.get(this.indice);
			int nivel;
			nivel= torre.primerNivelPosible();		
			
			if(comprobarSiCae(nivel,torre.piezaRecomendada(nivel))==true){		//ACA SE CAE EL JENGA---------------			
				this.ganadores = ganador();
				System.out.println(torre.toString());
				System.out.println(ganadores);
			}
			else{
				torre.mover(nivel,torre.piezaRecomendada(nivel));
				System.out.println(torre.toString());
			}
			
			if(this.indice==this.jugadores.size()-1){
				this.indice = 0;
			}else
				this.indice++;
		}
	}
	
	public ArrayList<String> ganador(){
		this.jugadores.remove(this.indice);
		ganadores = this.jugadores;
		return ganadores;
	}
	
	public boolean comprobarSiCae(int numNivel, int palito){		//COMRPUEBA SI SE CAE EL JENGA------------------------------
		int aux=0;
		if(torre.cantPalitos(numNivel)==3){
			switch (palito) {
				case 0 :
					aux=1*(torre.tama�o()-numNivel+1);
					break;
				case 1 :
					aux = 0;
					break;
				case 2 :
					aux=1*(torre.tama�o()-numNivel+1);
					break;
			}
		}		
		if(torre.cantPalitos(numNivel)==2){
			switch (palito) {
				case 0 :
					if(this.torre.torre.get(numNivel).getPalIzq()==1){
						aux=5*(torre.tama�o()-numNivel+1);
					}else{
						aux=100;
					}
					break;
				case 1 :
					aux=100;
					break;
				case 2 :					
					if(this.torre.torre.get(numNivel).getPalDer()==1){
						aux=5*(torre.tama�o()-numNivel+1);
					}else{
						aux=100;
						}
					break;
			}
		}
		return seCae(aux);
	}
	
	public boolean seCae(int porcentaje){
		Random r = new Random();		//RANDOM DE 1 AL 100----------
		int randomInt = r.nextInt(100)+1;
		if(randomInt <= porcentaje){
			return true;
		}return false;
	}
	
	
	@Override
	public String toString() {
		return this.torre.toString();
	}
	
	//FUNCIONES PARA EL TEST---------------------------------------------------------------------------
	
	public void quitar(int nivel,int palito){		
		if(comprobarSiCae(nivel,palito)==true){		//ACA SE CAE EL JENGA---------------			
			this.jugadores.remove(this.JugActual);
			ganadores=this.jugadores;			
		}
		torre.mover(nivel,palito);
	}
	public int primerNivelPosible(){
		return torre.primerNivelPosible();
	}
	public int altura(){
		return torre.tama�o();
	}
	
	public void a�adirPalito(){
		torre.a�adirPalito();
	}
	
}