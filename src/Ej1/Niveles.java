 package Ej1;

public class Niveles {
	
	private int palitoIzq;
	private int palitoDer;
	private int palitoCen;
	int Nivel;
	
	Niveles(int palitoIzq, int palitoDer, int palitoCen,int Nivel){		//CONSTRUCTOR-----------------------
		this.palitoIzq=palitoIzq;
		this.palitoDer=palitoDer;
		this.palitoCen=palitoCen;
		this.Nivel=Nivel;
	}
	
	public int getPalIzq(){
		return palitoIzq;
	}
	
	public int getPalDer(){
		return palitoDer;
	}
	
	public int getPalCen(){
		return palitoCen;
	}
	
	public int getNivel(){
		return Nivel;
	}
	
	
	void setPalIzq(int nuevoEstado){
		this.palitoIzq=nuevoEstado;
	}
	
	void setPalDer(int nuevoEstado){
		this.palitoDer=nuevoEstado;
	}
	
	void setPalCen(int nuevoEstado){
		this.palitoCen=nuevoEstado;
	}

	@Override
	public String toString() {
		StringBuilder mostrarNivel = new StringBuilder();
		mostrarNivel.append("Nivel "+ Nivel +" " + palitoIzq + palitoCen + palitoDer+"\n");
		return mostrarNivel.toString();
	}
	
	void setNivel(int nuevoNivel){
		this.Nivel=nuevoNivel;
	}

	
}
