package Ej2;

public class ABB extends  AB{
	
	private Integer cantNodos;
	private Nodo<Integer> raiz;
	public Integer getCantNodos(){
		return cantNodos;
	}
	
	//**************************************************************************
	public void insertar(int dato) {	//AGREGAR
        Nodo<Integer> nuevo = new Nodo<Integer>(dato);
        insertar(nuevo, raiz);
        actualizarAltura(raiz);
    }

    protected  void insertar(Nodo<Integer> nuevo, Nodo<Integer> pivote) {
        if (this.raiz == null) {
            raiz = nuevo;
            raiz.setAltura(1);
        } else {
            if (nuevo.getDato() <= pivote.getDato()) {
                if (pivote.getIzq() == null) {
                    pivote.setIzq(nuevo);
                } else {
                    insertar(nuevo, pivote.getIzq());
                }
            } else {
                if (pivote.getDer() == null) {
                    pivote.setDer(nuevo);
                } else {
                    insertar(nuevo, pivote.getDer());
                }
            }
        }

    }
	
    private void actualizarAltura(Nodo<Integer> n)
    {
    	if (n!=null)
    	{
    		n.setAltura(altura(n));
    		actualizarAltura(n.getIzq());
    		actualizarAltura(n.getDer());
    	}
    }
    
    //*****************************************************************
    public void eliminar(Integer elem){		//ELIMINAR
		if (raiz != null) 
			eliminar(elem, raiz);
	}
	
	protected Nodo<Integer> eliminar(Integer x, Nodo<Integer> p){
		if(p==null)
			return null;
		if(x==p.info){
			if(p.izq==null)
				return p.der;
			if(p.der==null)
				return p.izq;
			p.info=maxVal(p.izq);
			p.izq=eliminar(p.info,p.izq);
		}
		else if(x<p.info){
			p.izq=eliminar(x,p.izq);
		}else if(x>p.info){
			p.der=eliminar(x,p.der);
		}return p;
	}
	
	private int maxVal(Nodo<Integer> p){
		while(p.der !=null){
			p=p.der;
		}return p.info;
	}
    
	//******************************************************************
	public boolean balanceado(){		//BALANCEADO O(n*log(n))
		return (raiz == null) ? true : balanceado(raiz);
	}
	
	private boolean balanceado(Nodo<Integer> nodo){
		boolean ret= true;
		int altIzq = 0 ;
		int altDer = 0 ;
		if (nodo.izq != null){
			altIzq = altura(nodo.izq);
			ret = ret && balanceado(nodo.izq);
		}
		if (nodo.der != null){
			altDer = altura(nodo.der);
			ret = ret && balanceado(nodo.der);
		}
		ret=ret && Math.abs(altIzq - altDer) <= 1 ;
		return ret;
	}
	
	//*******************************************************************
	protected boolean irep(){		//IREP O(n)
		return cantNodos(raiz)==getCantNodos();
	}
	
	protected void romperIrep(){
		cantNodos++;
	}
	
	
	//******************************************************************
	public Nodo<Integer> iesimo(int i){		//IESIMO En el peor caso recrre o(n), ademas cantNodos recorre o(n). Es decir, iesimo = o(n^2)
		return iesimo(i,this.raiz);
	}
	
	private Nodo<Integer> iesimo(int x, Nodo<Integer> nodo){
		if(cantNodos(nodo.izq)+1 == x)
			return nodo;
		if(cantNodos(nodo.izq)+1 < x)
			return iesimo(x-(cantNodos(nodo.izq)+ 1), nodo.der); 
		else
			return iesimo(x,nodo.izq);
	}
	
	
	@Override
	public String toString(){
		return (raiz == null) ? "" : toString(raiz);
	}
	private String toString(Nodo<Integer> nodo){
		String ret = nodo.info.toString();
		if (nodo.izq != null) ret = ret + toString(nodo.izq);
		if (nodo.der != null) ret = ret + toString(nodo.der);
		return ret;
	}
	
	public int altura(){
		return (raiz == null) ? 0 : altura(raiz);
	}
	protected int altura(Nodo<Integer> nodo){
		int altIzq = (nodo.izq == null) ? 0 : altura(nodo.izq) ;
		int altDer = (nodo.der == null) ? 0 : altura(nodo.der) ;
		return 1 + Math.max(altIzq, altDer);
	}
	
	public int cantNodos(Nodo<Integer> nodo){
		if (nodo == null)
	          return 0;
		else
			return 1+cantNodos(nodo.izq)+cantNodos(nodo.der);
	}
	
}
