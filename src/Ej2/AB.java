package Ej2;

public class AB{
		
	public class Nodo<Integer>{		//CLASE INTERNA SIN METODOS----------
		Integer info;
		Nodo<Integer> izq;
		Nodo<Integer> der;
		int altura;
		
	public Nodo(Integer info){
	       this.info = info;
	       this.altura = 0;
	}
	
	public int getAltura() {
        return altura;
    }

    public void setAltura(int altura) {
        this.altura = altura;
    }
	
	public Integer getDato() {
        return info;
    }

    public void setDato(Integer info) {
        this.info = info;
    }

    public Nodo<Integer> getIzq() {
        return izq;
    }

    public void setIzq(Nodo<Integer> izq) {
        this.izq = izq;
    }

    public Nodo<Integer> getDer() {
        return der;
    }

    public void setDer(Nodo<Integer> der) {
        this.der = der;
    }
	
	@Override
	public String toString() {
			return info.toString();
		}
	}

}
